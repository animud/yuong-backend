from django.conf import settings
from django.core.mail import EmailMultiAlternatives

account_activation = """
    {}
"""


def send_email(email, code, type=None):
    if type is not None:
        html_content = account_activation.format(code)
        message = EmailMultiAlternatives('Yuong Activate Account', html_content, settings.EMAIL_HOST, [email])
    else:
        html_content = account_activation.format(code)
        message = EmailMultiAlternatives('Yuong Reset Password', html_content, settings.EMAIL_HOST, [email])

    message.attach_alternative(html_content, "text/html")

    status = None

    try:
        message.send()
        status = True
    except:
        status = False

    return status

