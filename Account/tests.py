import datetime

from django.test import TestCase
from django.utils import timezone

from Account.models import User


class UserTestCase(TestCase):

    def test_user_verified(self):
        time = timezone.now() + datetime.timedelta(days=7)
        user = User.objects.filter(is_verified=False, created_at__lt=time)
        self.assertEqual(list(user), [])