from django.core.management import BaseCommand

from Bot.views import Bot


class Command(BaseCommand):
    help = 'Run Yuong Bot'

    def handle(self, *args, **options):
        Bot()