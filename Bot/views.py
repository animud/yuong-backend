import telepot

from observable import Observable

from Bot.apps import BotConfig


class Bot:
    token = BotConfig.token
    url = BotConfig.url
    listener = Observable()
    bot = telepot.Bot(token)

    def sends(user, message=None, reply_id=None):
        Bot.bot.sendMessage(user, message, parse_mode='Markdown',
                            reply_to_message_id=reply_id)

    def __init__(self):
        self.bot.message_loop(handle)
        while 1:
            pass


events = {
    0: 'Bot.Reply',
    1: 'Bot.Command',
    2: 'Bot.Callback'
}

conditions = [
    lambda x: 'entities' not in x and 'data' not in x,
    lambda x: x.__contains__('entities'),
    lambda x: x.__contains__('data'),
]


def handle(message):
    automate_event(message)


def automate_event(msg):
    for item in enumerate(conditions):
        index = item[0]
        condition = item[1]

        print(events[index]) if condition(msg) else None

        # emit event
        Bot.listener.trigger(events[index], msg) if condition(msg) else None
