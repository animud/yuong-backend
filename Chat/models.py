from django.db import models


class Chat(models.Model):
    owner = models.ForeignKey('Account.User', related_name='chats', on_delete=models.CASCADE)
    members = models.ManyToManyField('Account.User', related_name='chat_members', blank=True)

    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    last_modified = models.DateTimeField(auto_now=True, blank=True, null=True)

    def __str__(self):
        return '{}: {}'.format(str(self.id), str(self.owner.email))


class Message(models.Model):
    text = models.TextField(max_length=250)
    chat = models.ForeignKey('Chat.Chat', related_name='chat_messages', on_delete=models.CASCADE, null=True)
    sent_at = models.DateTimeField(auto_now=True)
    sender = models.ForeignKey('Account.User', related_name='user_messages', on_delete=models.CASCADE, null=True)

    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    last_modified = models.DateTimeField(auto_now=True, blank=True, null=True)

    def __str__(self):
        return '{} - {}'.format(str(self.chat), str(self.sender))

