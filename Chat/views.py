from rest_framework import viewsets, mixins
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import IsAuthenticated, SAFE_METHODS
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK

from Chat.models import Chat, Message
from Chat.serializers import ChatSerializer, ChatReadSerializer, MessageSerializer, MessageReadSerializer
from Realtime.managers import Manager


class ChatViewSet(viewsets.ModelViewSet):
    queryset = Chat.objects.all()
    serializer_class = ChatSerializer
    permission_classes = [IsAuthenticated]

    def create(self, request, *args, **kwargs):
        serializer = ChatSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        chat = serializer.save()

        serializer = ChatReadSerializer(chat, context={'request': request})
        Manager.send({
            "data": {
                "chat": serializer.data,
            },
            "event": "Chat.Created",
        }, Manager.Channel.USER_CHANNEL)

        return Response(serializer.data, status=HTTP_200_OK)


class MessageViewSet(mixins.CreateModelMixin, mixins.ListModelMixin, viewsets.GenericViewSet):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer
    permission_classes = [IsAuthenticated]

    def get_serializer_class(self):
        if self.request.method in SAFE_METHODS:
            return MessageReadSerializer

        return self.serializer_class

    def get_queryset(self):
        if self.request.method in SAFE_METHODS:
            chat = self.request.GET.get('chat')
            if chat is None:
                raise ValidationError({'status': 0})
            return self.queryset.filter(chat=chat).order_by('sent_at')
        return self.queryset

    def create(self, request, *args, **kwargs):
        serializer = MessageSerializer(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        chat = serializer.validated_data.get('chat')
        text = serializer.validated_data.get('text')
        message = Message.objects.create(sender=request.user, chat=chat, text=text)
        serializer = MessageReadSerializer(message)
        Manager.send({
            "data": {
                "message": serializer.data,
            },
            "event": "Message.Created",

        }, Manager.Channel.USER_CHANNEL)

        return Response(serializer.data, status=HTTP_200_OK)