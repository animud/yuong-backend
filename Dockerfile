FROM python:3
ENV PYTHONBUFFERED 1
RUN mkdir /yuong
WORKDIR /yuong
ADD . /yuong

RUN apt-get update \
    && apt-get install -y software-properties-common python-software-properties \
    && add-apt-repository -y ppa:ubuntugis/ubuntugis-unstable \
    && apt-get install -y tar git curl nano wget dialog net-tools build-essential binutils libproj-dev gdal-bin python-gdal python3-gdal \
    && apt-get install -y locales tar git curl nano wget dialog net-tools build-essential \
    && localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8 \
    && apt-get install --no-install-recommends -y -q python3 python3-pip python3-dev python3-setuptools \
    && apt-get install -y libzmq-dev libevent-dev libxml2-dev libxslt-dev python-dev zlib1g-dev \
    && apt-get install -y libtiff5-dev libjpeg62-turbo-dev zlib1g-dev \
    && apt-get install -y libpq-dev libfreetype6-dev liblcms2-dev libwebp-dev tcl8.6-dev tk8.6-dev python-tk \
    && apt-get autoremove -y ${BUILD_DEPS} \
    && apt-get clean
ENV LANG en_US.utf8

ADD requirements.txt /yuong/
RUN pip3 install --upgrade pip
RUN pip3 install -r /yuong/requirements.txt