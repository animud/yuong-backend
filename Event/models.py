from django.db import models

from Account.utils import PathAndRename


class Event(models.Model):
    title = models.CharField(max_length=250)
    owner = models.ForeignKey('Account.User', related_name='user_events', on_delete=models.CASCADE, blank=True, null=True)
    description = models.TextField()
    avatar = models.ImageField(upload_to=PathAndRename('events/'), blank=True)
    start_date = models.DateTimeField(blank=True, null=True)
    end_date = models.DateTimeField(blank=True, null=True)
    members = models.ManyToManyField('Account.User', blank=True)

    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    last_modified = models.DateTimeField(auto_now=True, blank=True, null=True)

    def __str__(self):
        return '{} - {}'.format(self.id, self.title)
