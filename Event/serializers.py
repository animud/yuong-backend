from rest_framework import serializers

from Account.models import User
from Account.serializers import UserResponseSerializer
from Event.models import Event


class EventSerializer(serializers.ModelSerializer):
    members = serializers.SerializerMethodField()

    class Meta:
        model = Event
        fields = '__all__'

    def get_members(self, instance):
        serializer = UserResponseSerializer(instance.members.all(), many=True)
        return serializer.data


class EventUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = ('title', 'description', 'avatar', 'start_date', 'end_date')
        extra_kwargs = {
            'title': {
                'required': False
            },
            'description': {
                'required': False
            },
            'avatar': {
                'required': False
            },
            'start_date': {
                'required': False
            },
            'end_date': {
                'required': False
            },
        }

class EventJoinSerializer(serializers.Serializer):
    user = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=True)
