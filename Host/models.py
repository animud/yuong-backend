from django.db import models


class Host(models.Model):
    owner = models.ForeignKey('Account.User', related_name='user_hosts', on_delete=models.CASCADE, blank=True, null=True)
    city = models.ForeignKey('Account.City', related_name='city_hosts', on_delete=models.CASCADE, blank=True, null=True)
    gallery = models.ManyToManyField('Account.Gallery', related_name='host_gallery', blank=True)
    description = models.TextField(blank=True)
    is_children_available = models.BooleanField(default=True)
    is_pet_available = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    last_modified = models.DateTimeField(auto_now=True, blank=True, null=True)

    def __str__(self):
        return '{} - {}'.format(str(self.owner_id), str(self.id))



