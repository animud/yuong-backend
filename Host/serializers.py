from drf_haystack.serializers import HaystackSerializer
from rest_framework import serializers

from Host.models import Host
from Host.search_indexes import HostIndex


class HostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Host
        fields = '__all__'


class HostSearchSerializer(HaystackSerializer):
    class Meta:
        index_classes = [HostIndex]
        fields = ('text', 'description', 'city', 'is_children_available', 'is_pet_available', 'autocomplete')


class HostUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Host
        fields = ('city', 'description', 'is_children_available', 'is_pet_available')