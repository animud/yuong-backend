from rest_framework.routers import DefaultRouter

from Host.views import HostViewSet, HostSearchViewSet

router = DefaultRouter()

router.register(r'host/search', HostSearchViewSet, base_name='host_search')
router.register(r'host', HostViewSet, base_name='host')