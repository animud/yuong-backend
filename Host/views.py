from drf_haystack.viewsets import HaystackViewSet
from rest_framework import viewsets, mixins
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST

from Host.models import Host
from Host.serializers import HostSerializer, HostUpdateSerializer, HostSearchSerializer


class HostViewSet(mixins.CreateModelMixin, mixins.ListModelMixin, mixins.UpdateModelMixin,
                  mixins.RetrieveModelMixin, mixins.DestroyModelMixin, viewsets.GenericViewSet):
    serializer_class = HostSerializer
    queryset = Host.objects.all()

    def get_permissions(self):
        if self.action in ['create', 'update', 'partial_update', 'destroy']:
            self.permission_classes = [IsAuthenticated]
        elif self.action in ['list', 'retrieve']:
            self.permission_classes = [AllowAny]
            
        return super(HostViewSet, self).get_permissions()

    def create(self, request, *args, **kwargs):
        serializer = HostSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        serializer.save(owner=request.user)

        return Response(serializer.data, status=HTTP_200_OK)

    def update(self, request, *args, **kwargs):
        serializer = HostUpdateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        host = Host.objects.get(id=kwargs['pk'])

        if host.owner != request.user:
            return Response({'status': 400, 'text': 'you don\'t have permission'},
                            status=HTTP_400_BAD_REQUEST)

        serializer.update(host, serializer.validated_data)

        return Response(serializer.data, status=HTTP_200_OK)

    def destroy(self, request, *args, **kwargs):
        host = Host.objects.get(id=kwargs['pk'])

        if host.owner != request.user:
            return Response({'status': 400, 'text': 'you don\'t have permission'},
                            status=HTTP_400_BAD_REQUEST)

        host.delete()

        return Response({'status': 200, 'text': 'successfully deleted'}, status=HTTP_200_OK)


class HostSearchViewSet(HaystackViewSet):
    index_models = [Host]
    serializer_class = HostSearchSerializer
    permission_classes = [AllowAny]

