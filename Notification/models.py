from django.db import models


class Notification(models.Model):
    class Type:
        REQUEST_RECEIVED = 0
        REQUEST_ACCEPTED = 1
        REQUEST_CANCELED = 2
        MESSAGE_RECEIVED = 3
        ACCOUNT_VERIFY = 4

    type = models.IntegerField(default=Type.REQUEST_RECEIVED)
    message = models.TextField(blank=True)
    trigger_request = models.ForeignKey('Request.Request', related_name='triggered_notifications', on_delete=models.CASCADE, null=True)
    trigger_message = models.ForeignKey('Chat.Message', related_name='triggered_messages', on_delete=models.CASCADE, null=True)
    received_from = models.ForeignKey('Account.User', related_name='received_from', on_delete=models.CASCADE, null=True)
    send_to = models.ForeignKey('Account.User', related_name='send_to', on_delete=models.CASCADE, null=True)

    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    last_modified = models.DateTimeField(auto_now=True, blank=True, null=True)

    def __str__(self):
        return '{} - {}'.format(self.id, self.type)