from rest_framework.routers import DefaultRouter

from Notification.views import NotificationViewSet

router = DefaultRouter()

router.register(r'notification', NotificationViewSet, base_name='notification')