from rest_framework import viewsets, mixins
from rest_framework.permissions import AllowAny, IsAuthenticated

from Notification.models import Notification
from Notification.serializers import NotificationSerializer


class NotificationViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin,
                          mixins.DestroyModelMixin, viewsets.GenericViewSet):
    serializer_class = NotificationSerializer
    queryset = Notification.objects.all()
    permission_classes = [IsAuthenticated]
