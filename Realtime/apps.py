from django.apps import AppConfig


class RealtimeConfig(AppConfig):
    name = 'Realtime'

    def ready(self):
        from Realtime.managers import Manager
        Manager.send({}, Manager.Channel.ADMIN_CHANNEL)