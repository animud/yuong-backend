from rest_framework.routers import DefaultRouter

from Request.views import RequestViewSet

router = DefaultRouter()

router.register(r'request', RequestViewSet, base_name='request')
