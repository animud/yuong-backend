from django.db import models


class Travel(models.Model):
    user = models.ForeignKey('Account.User', related_name='user_travels', on_delete=models.CASCADE, blank=True)
    city = models.ForeignKey('Account.City', related_name='city_travels', on_delete=models.CASCADE, blank=True)
    host = models.ForeignKey('Host.Host', related_name='host_travels', on_delete=models.CASCADE, blank=True)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()

    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    last_modified = models.DateTimeField(auto_now=True, blank=True, null=True)

    def __str__(self):
        return str(self.id)

