from rest_framework import serializers

from Travel.models import Travel


class TravelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Travel
        fields = '__all__'


class TravelUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Travel
        exclude = ('id', 'user')