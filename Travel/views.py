from rest_framework import viewsets, mixins
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_200_OK

from Travel.models import Travel
from Travel.serializers import TravelSerializer, TravelUpdateSerializer


class TravelViewSet(mixins.CreateModelMixin, mixins.ListModelMixin, mixins.RetrieveModelMixin,
                    mixins.UpdateModelMixin, mixins.DestroyModelMixin, viewsets.GenericViewSet):
    serializer_class = TravelSerializer
    queryset = Travel.objects.all()

    def get_permissions(self):
        if self.action in ['create', 'update', 'partial_update', 'destroy']:
            self.permission_classes = [IsAuthenticated]
        elif self.action in ['list', 'retrieve']:
            self.permission_classes = [AllowAny]

        return super(TravelViewSet, self).get_permissions()

    def update(self, request, *args, **kwargs):
        serializer = TravelUpdateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        travel = Travel.objects.get(id=kwargs['pk'])

        if travel.user != request.user:
            return Response({'status': 400, 'text': 'you don\'t have permission'},
                            status=HTTP_400_BAD_REQUEST)

        serializer.update(travel, serializer.validated_data)

        return Response(serializer.data, status=HTTP_200_OK)

    def destroy(self, request, *args, **kwargs):
        travel = Travel.objects.get(id=kwargs['pk'])

        if travel.user != request.user:
            return Response({'status': 400, 'text': 'you don\'t have permission'},
                            status=HTTP_400_BAD_REQUEST)

        travel.delete()

        return Response({'status': 200, 'text': 'successfully deleted'}, status=HTTP_200_OK)
