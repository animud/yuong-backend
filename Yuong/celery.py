import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings.development')

app = Celery('Yuong')

app.config_from_object('Yuong.celery_config')
app.autodiscover_tasks()