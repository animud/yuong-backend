broker_url = 'rpc://guest:guest@rabbitmq:5672/'
result_backend = 'rpc://guest:guest@rabbitmq:5672/'

result_serializer = 'json'
accept_content = ['application/json']
enable_utc = True
timezone = 'UTC'