import os

bind = '0.0.0.0:8000'
default_workers = 1
workers = os.environ.get('WEB_CONCURRENCY', default_workers)
worker_class = 'tornado'
max_requests = 350
max_requests_jitter = 350
timeout = 200
errorlog = '-'