{{ object.email }}
{{ object.first_name }}
{{ object.last_name }}
{{ object.birth_date }}
{{ object.phone }}
{% for friend in object.friends_list.all %}
    {{ friend.email }}
    {{ friend.first_name }}
    {{ friend.last_name }}
    {{ friend.birth_date }}
    {{ friend.phone }}
    {{ friend.user_city.city_name }}
    {{ friend.telegram_name }}
{% endfor %}
{{ object.user_city.city_name }}
{{ object.telegram_name }}